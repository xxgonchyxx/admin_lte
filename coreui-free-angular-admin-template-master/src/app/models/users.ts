export class Users {
    _id: string;
    username: {
        type: String,
        unique: true,
        required: true
    };
    password: {
        type: String,
        required: true
    };
    role: {
        type: String,
        default: 'Cajero'
    };
    status: {
        type: Number,
        default: 1
    };

    timestamps: true
}
