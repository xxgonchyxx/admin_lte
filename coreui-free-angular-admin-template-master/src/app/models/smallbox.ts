export class Smallbox {

    constructor(_id='', docType='',nroDoc=0,distribuitor='',user='',concept='',
                amount=0){
            this._id=_id;
            this.docType=docType;
            this.nroDoc=nroDoc;
            this.distribuitor=distribuitor;
            this.user=user;
            this.concept=concept;
            this.amount=amount;
    }

    _id: string;
    docType: string;
    nroDoc: number;
    distribuitor:string;
    user:string;
    concept:string;
    amount:number;
}
