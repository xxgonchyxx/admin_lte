export class Reports {
    _id:string;

    bill: {
        type: String,
        unique: true,
        required: true
    };
    receipt: {
        type: String,
        required: true
    }; 
    
}
