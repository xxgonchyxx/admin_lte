export class Expenses {
    _id:string;
    name: {
        type: String,
        unique: true,
        required: true
    };
    description: {
        type: String,
        required: true
    }; 
    timestamps: true
}


