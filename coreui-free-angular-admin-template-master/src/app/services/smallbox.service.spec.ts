import { TestBed } from '@angular/core/testing';

import { SmallboxService } from './smallbox.service';

describe('SmallboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SmallboxService = TestBed.get(SmallboxService);
    expect(service).toBeTruthy();
  });
});
