import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Roles } from '../models/roles';

@Injectable({
  providedIn: 'root'
})
export class RolesService {
  selectedRoles:Roles;
  roles: Roles[];
  readonly URL_API= 'http://localhost:3700/api/roles';

  constructor(private http:HttpClient) { 
    this.selectedRoles= new Roles();
  }

  getRoles() {
    return this.http.get(this.URL_API);
    
  }

  postRoles(Roles:Roles){
    return this.http.post(this.URL_API, Roles);
  }
  putRoles(roles:Roles){
    return this.http.put(this.URL_API + `/${roles._id}`,roles);
  }
  deleteRoles(_id:string){
    return this.http.delete(this.URL_API + `/${_id}`);
  }
}
