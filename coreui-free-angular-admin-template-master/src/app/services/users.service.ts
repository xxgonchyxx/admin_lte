import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Users } from '../models/users';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  selectedUsers:Users;
  
  users: Users[];
  readonly URL_API= 'http://localhost:3700/api/users';
  constructor(private http:HttpClient) { 
    this.selectedUsers= new Users();
  }

  getUsers() {
    return this.http.get(this.URL_API);
    
  }

  postUsers(Users: Users){
    return this.http.post(this.URL_API, Users);
  }
  putUsers(users:Users){
    return this.http.put(this.URL_API + `/${users._id}`,users);
  }
  deleteUsers(_id:string){
    return this.http.delete(this.URL_API + `/${_id}`);
  }
}
