import { Injectable } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import { Expenses } from '../models/expenses';

@Injectable({
  providedIn: 'root'
})
export class ExpensesService {
selectedExpenses:Expenses;
expenses: Expenses[];

readonly URL_API= 'http://localhost:3700/api/expenses';
  constructor(private http:HttpClient) { 
    this.selectedExpenses= new Expenses();
  }

  getExpenses(){
    return this.http.get(this.URL_API);
  }

  postExpenses(Expenses:Expenses){ 
    return this.http.post(this.URL_API, Expenses);
  }
  putExpenses(expenses:Expenses){
    return this.http.put(this.URL_API + `/${expenses._id}`,expenses);
  }
  deleteExpenses(_id:string){
    return this.http.delete(this.URL_API + `/${_id}`);
  }
}
