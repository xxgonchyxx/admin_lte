import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Smallbox } from '../models/smallbox';
import {SmallBoxComponent} from '../components/small-box/small-box.component';

@Injectable({
  providedIn: 'root'
})
export class SmallboxService {

  selectedSmallBox: Smallbox;
  smallBox: Smallbox[];
  readonly URL_API= 'http://localhost:3700/api/small-box';
  
  constructor(private http:HttpClient) {
    this.selectedSmallBox= new Smallbox();
   }

  getSmallBox() {
    return this.http.get(this.URL_API);
    
  }

  postSmallBox(Smallbox:Smallbox){
    return this.http.post(this.URL_API, Smallbox);
  }
  putSmallbox(smallbox:Smallbox){
    return this.http.put(this.URL_API + `/${smallbox._id}`,smallbox);
  }
  deleteSmallBox(_id:string){
    return this.http.delete(this.URL_API + `/${_id}`);
  }
}
