import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Reports } from '../models/reports';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  selectedReports:Reports;
  reports: Reports[];
  readonly URL_API= 'http://localhost:3700/api/reports';

  constructor(private http:HttpClient) { 
    this.selectedReports= new Reports();
  }
  getExpenses() {
    return this.http.get(this.URL_API);
    
  }

  postExpenses(Reports:Reports){
    return this.http.post(this.URL_API, Reports);
  }
  /** 
  putExpenses(reports:Reports){
    return this.http.put(this.URL_API + `/${reports._id}`,reports);
  }
  deleteExpenses(_id:string){
    return this.http.delete(this.URL_API + `/${_id}`);
  }
  */
}
