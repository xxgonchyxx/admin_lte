import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ButtonsModule } from 'ngx-bootstrap/buttons';
import {RolesComponent} from './roles.component';


@NgModule({
  imports: [
    FormsModule,
    
    
    ButtonsModule.forRoot()
  ],
  declarations: [ RolesComponent ]
})
export class DashboardModule { }
