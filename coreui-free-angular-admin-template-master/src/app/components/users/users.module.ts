import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ButtonsModule } from 'ngx-bootstrap/buttons';
import {UsersComponent} from './users.component';


@NgModule({
  imports: [
    FormsModule,
    
    
    ButtonsModule.forRoot()
  ],
  declarations: [ UsersComponent ]
})
export class DashboardModule { }
