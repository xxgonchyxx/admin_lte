import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import {SmallboxService} from '../../services/smallbox.service';
import {NgForm} from '@angular/forms';
import { from } from 'rxjs';
import { Smallbox } from '../../models/smallbox';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

//aca iria materialize

@Component({
  selector: 'app-small-box',
  templateUrl: './small-box.component.html',
  styleUrls: ['./small-box.component.scss'],

  providers:[SmallboxService]

})
export class SmallBoxComponent implements OnInit {

  //smallbox:any;
  //smallboxs: any[]=[
      //{id:1, docType: 'txt',nroDoc:'1',distribuitor:'gonchy',
      // user:'gonchy', concept:'asdfghj', amount:'0'}
  //];

  constructor(private smallboxService: SmallboxService) {} 
    //private modalService: NgbModal ) {}
  //@ViewChild('btnClose') btnClose: ElementRef;
  
  //modlas
  //ver(smallbox: any, modal) {
   // this.smallbox = smallbox;
   // this.modalService.open(modal);
  //}

  ngOnInit() {
    this.getSmallBox();
  }

  //metodos
  addSmallBox(form: NgForm){
    //console.log(form.value);
    if(form.value._id){
      this.smallboxService.putSmallbox(form.value)
      .subscribe(res=>{
        //console.log(res);
        this.resetForm(form);
        //aca iria el toaster
  
        //fin toaster  
        this.getSmallBox(); 
      });
    }else{
      this.smallboxService.postSmallBox(form.value)
      .subscribe(res => {
        console.log(res);
        this.resetForm(form);
        //aca iria el toaster
        //toastr.success('Added Successfull');
        //fin toaster  
        this.getSmallBox();         
      });
    }
  }

  getSmallBox(){
    this.smallboxService.getSmallBox()
    .subscribe(res => {
      this.smallboxService.smallBox = res as Smallbox[];
      console.log(res);
    });
  }

  editSmallBox(smallbox: Smallbox){
    this.smallboxService.selectedSmallBox= smallbox;

  }
  

  deleteSmallBox(_id:string){
    if(confirm('Are you sure of delete?')){ 
      this.smallboxService.deleteSmallBox(_id)
      .subscribe(res=> {
        this.getSmallBox();
        //aca tosater
      });      
      
    }

    
  }

  resetForm(form?: NgForm){
    if( form){
      form.reset();
      this.smallboxService.selectedSmallBox= new Smallbox();
    }
  }

  /** 
  //modals
  preeditSmallBox(smallBox:Smallbox){
    this.smallboxService.getSmallBox()
    .subscribe(res => {
      this.smallboxService.selectedSmallBox=res as Smallbox;
    });    
  }
  onSave(form?: NgForm): void{
    console.log(form.value.id);
    if(form.value.id == null){
      this.smallboxService.putSmallbox(form.value);
    }else{
      this.smallboxService.postSmallBox(form.value);
    }
    this.resetForm(form);
    this.btnClose.nativeElement.click();
  }
  */
}
