import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ButtonsModule } from 'ngx-bootstrap/buttons';
import {ReportsComponent} from './reports.component';


@NgModule({
  imports: [
    FormsModule,
    
    
    ButtonsModule.forRoot()
  ],
  declarations: [ ReportsComponent ]
})
export class DashboardModule { }
