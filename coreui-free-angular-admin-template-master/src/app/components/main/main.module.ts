import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ButtonsModule } from 'ngx-bootstrap/buttons';
import {MainComponent} from './main.component';


@NgModule({
  imports: [
    FormsModule,
    
    
    ButtonsModule.forRoot()
  ],
  declarations: [ MainComponent ]
})
export class DashboardModule { }
