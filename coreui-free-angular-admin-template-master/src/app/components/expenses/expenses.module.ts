import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ButtonsModule } from 'ngx-bootstrap/buttons';
import {ExpensesComponent} from './expenses.component';


@NgModule({
  imports: [
    FormsModule,
    
    
    ButtonsModule.forRoot()
  ],
  declarations: [ ExpensesComponent ]
})
export class DashboardModule { }
